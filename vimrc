" load
"-----------------------------------------------------------------
fun SetupVAM()
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME') . '/.vim/vim-addons'
  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
  " let g:vim_addon_manager = { your config here see "commented version" example and help
  if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
    execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
                \       shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
  endif
  call vam#ActivateAddons([
        \ 'The_NERD_tree',
        \ 'The_NERD_Commenter',
        \ 'unimpaired',
        \ 'vim-stylus',
        \ 'jade',
        \ 'vim-coffee-script',
        \ 'github:garbas/vim-snipmate',
        \ 'github:vim-scripts/Colour-Sampler-Pack',
        \ 'github:scrooloose/syntastic',
        \ 'github:Chiel92/vim-autoformat.git',
        \ 'github:pangloss/vim-javascript',
        \ 'github:jistr/vim-nerdtree-tabs',
        \ 'github:terryma/vim-multiple-cursors',
        \ 'github:kien/ctrlp.vim',
        \ 'github:Lokaltog/powerline',
        \ 'github:szw/vim-ctrlspace'
        \ ], {'auto_install' : 1})
endfun
call SetupVAM()

" Basic config
"-----------------------------------------------------------------
set nocompatible
syn on                          " syntax highlighting
filetype indent on              " activates indenting for files
filetype plugin on
set nu                          " line numbers
set hlsearch                    " highlight what you search for
set incsearch                   " type-ahead-find
set expandtab                   " use spaces, not tabs
set smarttab                    " make <tab> and <backspace> smarter
set tabstop=2                   " tabstops of 4
set shiftwidth=2                " indents of 4
set backspace=eol,start,indent  " allow backspacing over indent, eol, & start
set complete=.,w,b,u,U,t,i,d    " do lots of scanning on tab completion
set fileencoding=utf-8          " encoding utf8
set ffs=unix
set guioptions-=T               " Remove toolbar
set browsedir=buffer
set ofu=syntaxcomplete#Complete
set hid
set history=400                 "Sets how many lines of history VIM har to remember
set nowrap
set cc=80

" filetype indent
autocmd FileType python setlocal shiftwidth=4 softtabstop=4

" Error tab
match errorMsg /[^\t]\zs\t\+/

"Set to auto read when a file is changed from the outside
set autoread

"Have the mouse enabled all the time:
set mouse=a

"Set mapleader
let mapleader = ","
let g:mapleader = ","

"Do not redraw, when running macros.. lazyredraw
set lz

"Bbackspace and cursor keys wrap to
set whichwrap+=<,>,h,l

"Ignore case when searching
set ignorecase
set incsearch
set smartcase

"Set magic on
set magic

"No sound on errors.
set noerrorbells
set novisualbell
set t_vb=

"show matching bracets
set showmatch

"How many tenths of a second to blink
set mat=2

"Highlight search things
set hlsearch

"Remove the Windows ^M
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

"Remove indenting on empty lines
map <F2> :%s/\s*$//g<cr>:noh<cr>''

"Change dos to unix fileformat
map <Leader>unix :e ++ff=dos<cr>:setlocal ff=unix<cr><cr>


"Reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

" Styles
"---------------------------------------------------------------------------
set cursorline
set background=light
color solarized

hi CtrlSpaceSelected term=reverse ctermfg=187  ctermbg=23  cterm=bold
hi CtrlSpaceNormal   term=NONE    ctermfg=244  ctermbg=232 cterm=NONE
hi CtrlSpaceFound    ctermfg=220  ctermbg=NONE cterm=bold
hi CtrlSpaceStatus   ctermfg=230  ctermbg=234  cterm=NONE

" Files and Backups
"---------------------------------------------------------------------------
set nobackup
set nowb
set noswapfile


" Folding
"--------------------------------------------------------------------------
set foldmethod=indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use


" save CTRL-S
"--------------------------------------------------------------------------
nmap <C-s> :w<CR>
imap <C-s> <Esc>:w<CR>a

" window split 
"--------------------------------------------------------------------------
noremap <silent>,hs :vsplit<CR>
noremap <silent>,s :split<CR>

" Easy split navigation
"--------------------------------------------------------------------------
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" window control 
"--------------------------------------------------------------------------
function Maxwin()
  :res 9999<CR>
  :vertical res 9999<CR>
endfunction

nnoremap <C-w-_> :res 9999<CR>
noremap <silent>,m :call Maxwin()<CR>
noremap <silent>,0 :<C-w-=><CR>

" NERDTree
"---------------------------------------------------------------------------
noremap <silent>,n :NERDTreeToggle<CR>
noremap <silent>,fn :NERDTreeFind<CR>
let NERDTreeIgnore=['\~$','\.pyc$']
let g:nerdtree_tabs_open_on_gui_startup=0
"let g:nerdtree_tabs_autoclose=0

" Tab nav
"---------------------------------------------------------------------------
nnoremap <C-TAB> :tabn<CR>
noremap <CS-TAB> :tabp<CR>

" Bubble single lines
"---------------------------------------------------------------------------
nmap <C-k> [e
nmap <C-j> ]e


" Bubble multiple lines
"---------------------------------------------------------------------------
vmap <C-J> ]egv
vmap <C-k> [egv

" Visually select the text that was last edited/pasted
"---------------------------------------------------------------------------
nmap gV `[v`]


" functions
"---------------------------------------------------------------------------

" Statusline
"-----------------------------------------------------------------------------------------------------
  "Always hide the statusline
  set laststatus=2

  "Format the statusline
  set statusline=\ %F%m%r%h\ %w\ Line:\ %l/%L:%c\ [%{&ff}]\ [%p%%]

  call vam#ActivateAddons(['powerline'])

" Only have cursorline in current window
autocmd WinLeave * set nocursorline
autocmd WinEnter * set cursorline

" Visual
" From an idea by Michael Naumann
"-----------------------------------------------------------------------------------------------------
function! VisualSearch(direction) range
  let l:saved_reg = @"
  execute "normal! vgvy"
  let l:pattern = escape(@", '\\/.*$^~[]')
  let l:pattern = substitute(l:pattern, "\n$", "", "")
  if a:direction == 'b'
    execute "normal ?" . l:pattern . "^M"
  else
    execute "normal /" . l:pattern . "^M"
  endif
  let @/ = l:pattern
  let @" = l:saved_reg
endfunction

"Basically you press * or # to search for the current selection !! Really useful
vnoremap <silent> * :call VisualSearch('f')<CR>
vnoremap <silent> # :call VisualSearch('b')<CR>

function Entities()
  silent s/À/\&Agrave;/eg
  silent s/Á/\&Aacute;/eg
  silent s/Â/\&Acirc;/eg
  silent s/Ã/\&Atilde;/eg
  silent s/Ä/\&Auml;/eg
  silent s/Å/\&Aring;/eg
  silent s/Æ/\&AElig;/eg
  silent s/Ç/\&Ccedil;/eg
  silent s/È/\&Egrave;/eg
  silent s/É/\&Eacute;/eg
  silent s/Ê/\&Ecirc;/eg
  silent s/Ë/\&Euml;/eg
  silent s/Ì/\&Igrave;/eg
  silent s/Í/\&Iacute;/eg
  silent s/Î/\&Icirc;/eg
  silent s/Ï/\&Iuml;/eg
  silent s/Ð/\&ETH;/eg
  silent s/Ñ/\&Ntilde;/eg
  silent s/Ò/\&Ograve;/eg
  silent s/Ó/\&Oacute;/eg
  silent s/Ô/\&Ocirc;/eg
  silent s/Õ/\&Otilde;/eg
  silent s/Ö/\&Ouml;/eg
  silent s/Ø/\&Oslash;/eg
  silent s/Ù/\&Ugrave;/eg
  silent s/Ú/\&Uacute;/eg
  silent s/Û/\&Ucirc;/eg
  silent s/Ü/\&Uuml;/eg
  silent s/Ý/\&Yacute;/eg
  silent s/Þ/\&THORN;/eg
  silent s/ß/\&szlig;/eg
  silent s/à/\&agrave;/eg
  silent s/á/\&aacute;/eg
  silent s/â/\&acirc;/eg
  silent s/ã/\&atilde;/eg
  silent s/ä/\&auml;/eg
  silent s/å/\&aring;/eg
  silent s/æ/\&aelig;/eg
  silent s/ç/\&ccedil;/eg
  silent s/è/\&egrave;/eg
  silent s/é/\&eacute;/eg
  silent s/ê/\&ecirc;/eg
  silent s/ë/\&euml;/eg
  silent s/ì/\&igrave;/eg
  silent s/í/\&iacute;/eg
  silent s/î/\&icirc;/eg
  silent s/ï/\&iuml;/eg
  silent s/ð/\&eth;/eg
  silent s/ñ/\&ntilde;/eg
  silent s/ò/\&ograve;/eg
  silent s/ó/\&oacute;/eg
  silent s/ô/\&ocirc;/eg
  silent s/õ/\&otilde;/eg
  silent s/ö/\&ouml;/eg
  silent s/ø/\&oslash;/eg
  silent s/ù/\&ugrave;/eg
  silent s/ú/\&uacute;/eg
  silent s/û/\&ucirc;/eg
  silent s/ü/\&uuml;/eg
  silent s/ý/\&yacute;/eg
  silent s/þ/\&thorn;/eg
  silent s/ÿ/\&yuml;/eg
  silent s/¡/\&iexcl;/eg
  silent s/¿/\&iquest;/eg
  silent s/"/\&quot;/eg
  silent s/º/\&ordm;/eg
endfunction
map <Leader>h :call Entities()<CR>

"-----------------------------------------------------------------------------------------------------
" snipmate
"-----------------------------------------------------------------------------------------------------
let g:snippets_dir = '~/.vim/snippets'

"-----------------------------------------------------------------------------------------------------
" syntastic
"-----------------------------------------------------------------------------------------------------
map <C-ENTER> :SyntasticCheck<CR>

"-----------------------------------------------------------------------------------------------------
" 
"-----------------------------------------------------------------------------------------------------
let g:formatprg_args_expr_javascript = '"-p -i -s 2 -b expand -k"'

"-----------------------------------------------------------------------------------------------------
" make 
"-----------------------------------------------------------------------------------------------------
autocmd FileType python set makeprg=python\ % 
autocmd FileType javascript set makeprg=node\ % 
autocmd FileType sh set makeprg=sh\ % 


"-----------------------------------------------------------------------------------------------------
" ctrlp 
"-----------------------------------------------------------------------------------------------------
map fb :CtrlPMRU<cr>
map ff :CtrlP<cr>

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll|pyc)$'
  \ }
